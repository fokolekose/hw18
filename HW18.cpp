﻿#include <iostream>

template<typename T>
class Stack
{
private:
	int size = 0;
	T *arr = new T[size];
	
public:
	void Push(T value)
	{
		int newArrSize = size + 1;
		T* newArr = new T[newArrSize];
		if (size == 0)
		{
			newArr[0] = value;
		}
		else
		{
			for (int i = 0; i < size; i++)
			{
				newArr[i] = arr[i];
			}
			newArr[size] = value;
		}
		size = newArrSize;
		delete[] arr;
		arr = newArr;
	}

	void Pop()
	{
		if (size == 0)
		{
			std::cout << "Error: The array is empty!" << "\n";
		}
		else
		{
			int newArrSize = size - 1;
			T* newArr = new T[newArrSize];
			for (int i = 0; i < newArrSize; i++)
			{
				newArr[i] = arr[i];
			}
			size = newArrSize;
			delete[] arr;
			arr = newArr;
		}
	}

	void ShowArray()
	{
		if (size == 0)
		{
			std::cout << "Error: The array is empty!" << "\n";
		}
		else
		{
			for (int i = 0; i < size; i++)
			{
				std::cout << arr[i] << "\t";
			}
			std::cout << "\n";
		}
	}

	void ShowElement(int index)
	{
		if (size == 0)
		{
			std::cout << "Error: The array is empty!" << "\n";
		}
		else if (index >= size)
		{
			std::cout << "Error: The last index of the array is "
				<< size - 1 << "!" << "\n";
		}
		else
		{
			std::cout << arr[index] << "\n";
		}		
	}
};

int main()
{
	Stack<int> stack;
	stack.Pop();
	stack.ShowArray();
	stack.ShowElement(3);
	stack.Push(1);
	stack.Push(2);
	stack.Push(3);
	stack.Push(4);
	stack.ShowElement(4);
	stack.ShowElement(3);
	stack.ShowElement(1);
	stack.ShowArray();
	stack.Pop();
	stack.ShowArray();
	stack.Pop();
	stack.ShowArray();
	stack.Pop();
	stack.ShowArray();
	stack.Pop();
	stack.ShowArray();
	stack.Pop();

	return 0;
}